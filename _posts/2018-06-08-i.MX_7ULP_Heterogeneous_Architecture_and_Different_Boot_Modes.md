---
title: i.MX 7ULP Heterogeneous Architecture and Different Boot Modes
date: 2018-06-08 12:03:40
author: vanessa
categories: [ tutorial ]
tags: [ boot, IMX7ULP, m4 ]
---

The i.MX 7ULP is an heterogeneous SoC that presents a power domain architecture
different than the other i.MX SoCs. In this architecture, both cores have control
of the system power and this allows booting in different modes by changing the
primary and auxiliary cores according to the power saving strategy.

This post presents the three boot modes supported by i.MX 7ULP and how to boot in
each one of them.

# Boot modes

* **Dual Boot**: M4 works as the primary core, loading from QSPI flash and booting
A7 as the auxiliary core from eMMC/SD. This is the **default** boot mode for i.MX
7ULP.

* **Low Power Boot**: M4 works as the primary core, loading from QSPI flash and
booting A7 on demand.

* **Single Boot**: A7 works as the primary core controlling the whole system and
booting M4. This is **not** the typical boot mode for the i.MX 7ULP.

# Source code

The U-Boot source code for i.MX 7ULP can be found at
[uboot-imx](https://source.codeaurora.org/external/imx/uboot-imx/log/?h=imx_v2017.03_4.9.88_2.0.0_ga).

```
$ git clone https://source.codeaurora.org/external/imx/uboot-imx
$ cd uboot-imx
$ git checkout imx_v2017.03_4.9.88_2.0.0_ga
```

The SDK package for the i.MX 7ULP Cortex-M4 can be found at
[MCUXpresso](https://mcuxpresso.nxp.com/en/welcome).

{: .notice--info}
To learn how to download and build the SDK package for M4, please see
[this](https://imxdev.gitlab.io/tutorial/How_to_build_and_flash_a_bootable_image_for_Cortex-M4_on_i.MX_7ULP/) post.

## Dual Boot

To boot on `Dual Boot` (**default**), make sure that `SW1` is set as `1001`.

<img src="{{ absolute_url }}/images/ulp_dual_boot.jpg">

M4 loads from QSPI flash and A7 loads from SD card.

## Low Power Boot

To boot on `Low Power Boot`, make sure that `SW1` is set as `101x`.

<img src="{{ absolute_url }}/images/ulp_lp_boot.jpg">

Only M4 boots on this mode and M4 can be programmed to boot A7 if needed (on
demand). In this boot mode, the M4 image must be already flashed to QSPI.

## Single Boot

To boot on `Single Boot`, make sure that `SW1` is set as `1000`.

<img src="{{ absolute_url }}/images/ulp_single_boot.jpg">

In this boot mode, the M4 image is loaded from U-Boot. To build the U-Boot image
with the M4 content, follow these commands:

- First, copy the the M4 bootable image (`.img` file) to the `uboot-imx`
directory and rename it to `ulp_m4.bin`.

```
$ cp <sdk_dir>/boards/evkmcimx7ulp/demo_apps/power_mode_switch/power_mode_switch.img \
<uboot-imx_dir>/ulp_m4.bin
```

{: .notice--info}
For details on how to get the bootable image for M4, please see
[this](https://imxdev.gitlab.io/tutorial/How_to_build_and_flash_a_bootable_image_for_Cortex-M4_on_i.MX_7ULP/) post.

- Build U-Boot with the following configuration:

```
$ export ARCH=arm
$ export CROSS_COMPILE=/usr/bin/arm-linux-gnueabi-
$ make mx7ulp_evk_m4boot_defconfig
$ make
```

This generates `u-boot-dtb.imx`. Flash it to the SD Card and boot the board. The
board will boot on Single Boot.
