---
title: System Controller Unit (SCU) Introduction for i.MX 8QXP MEK
redirect_from: blog/tutorial/2018/11/07/system-controller-unit-introduction-for-imx-8qxp-mek/
date: 2018-11-13 11:04:56
author: andres
tags: [IMX8QXP, SCFW, SCU]
categories: [Tutorial]
---

The newly released i.MX 8QXP introduces a new concept for manipulating resource allocation, power, clocking and IO configuration and muxing. Due to the architecture complexity of this new chip, a System Controller Unit (SCU) has been added to the system. The SCU is a Arm Cortex-M4 core and is the first processor to boot in the i.MX 8QXP design. 

It is the responsability of the SCU to control:

* Boot management
* Power management
* Clock
* IO and reset management configuration and muxing
* Resource partitioning / access control
* DDR Management
* Temperature monitoring
* AP watchdog services
* Low power wakeup

All those subsystems are abstracted thanks to the System Controller Firmware (SCFW) running in the SCU. Other software components communicate with SCU via an Application Programming Interface (API).

This API makes Remote Procedure Calls (RPC) via an underlying Inter-Processor Communication (IPC) mechanism. Each OS distributed by NXP features a library that allows it to interact with the SCFW running on the SCU.

<img src="{{ site.url }}{{ site.baseurl }}/images/ipc-calls.png">

## SCU Architecture

The SCU sub-system is made out of:

* 1x Arm Cortex-M4 processor running at 266MHz with 256KB of Tightly Coupled Memory (TCM)

<img src="{{ site.url }}{{ site.baseurl }}/images/scu-architecture.png">

It includes the following set of peripherals:

* 1x TPM 
* 1x UART
* 1x I2C
* 8x GPIOs
* 4x MUs

## System Controller Firmware (SCFW)

In order to control all the functionality of the SCU, NXP has created the SCFW. The SCFW is distributed in a porting kit from.

NXP only releases packages in a majority object code form, however a board.c file is provided in which most of the configuration of the SCFW can be done.

The first main step from SCFW to configure the DDR and start all other cores in the system. The boot flow is described in the image below:

<img src="{{ site.url }}{{ site.baseurl }}/images/boot-flow-imx8.png">

The i.MX8 boot sequence involves SCU ROM, SCFW, Security Controller (SECO) ROM, and SECO FW:

* At reset, the SCU ROM and SECO ROM both start execution
* The SCU ROM reads boot mode pins
* SCU ROM loads the first container from the boot media; this container always has the SECO FW, signed using the NXP key
* The SECO FW is loaded into SECO Tightly Coupled Memory (TCM)
* A message is sent by the SCU via private MU to the SECO ROM to authenticate and run SECO FW
* The SCU ROM loads the second container from the boot media; this container always has the SCFW and can be signed using the customer key
* SCFW is loaded to SCU TCM
* SCU ROM will then configure the DDR
* SCU ROM will start the SCFW

From this point SCFW takes over and loads any image to the Arm Cortex-M or Cortex-A cores.


### Downloading and building SCFW

Download the Arm GCC toolchain from here:

<https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads>

Export the toolchain to compile SCFW:

```console
mkdir ~/gcc_toolchain
cp ~/Downloads/gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2 ~/gcc_toolchain/
cd ~/gcc_toolchain/
tar -xvjf gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2
export TOOLS=~/gcc_toolchain/
```

The latest porting kit can be found here:

https://www.nxp.com/webapp/sps/download/license.jsp?colCode=L4.14.62_1.0.0_Beta_SCFWKIT&appType=file2&location=null&DOWNLOAD_ID=null&lang_cd=en

After the SCFW porting kit is downloaded and un-tar, navigate into `packages` and run the binary file inside:

```console
$ cd <download-dir>/packages/
$ chmod a+x imx-scfw-porting-kit-1.0.bin
$ ./imx-scfw-porting-kit-1.0.bin
```

Navigate inside the newly created "imx-scfw-porting-kit-0.7" and into "src". Extract the desired SCFW porting kit.

```console
$ cd imx-scfw-porting-kit-1.0/
$ tar -xvf src/scfw_export_mx8qx_b0.tar.gz
$ cd scfw_export_mx8qx_b0/
```

{: .notice--info}
The tar file extracted will be different depending on which board is being used.

All the code that is specific to a board configuration is under "platform/board/mx8qxp_mek/board.c"

To compile SCFW type the following command:

```console
$ make qx B=mek R=B0
```

All the files resulting from compilation can be found inside build_mx8qx/
