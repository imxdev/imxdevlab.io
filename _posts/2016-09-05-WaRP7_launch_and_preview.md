---
title: WaRP7 launch and preview
redirect_from: blog/new_boards/2016/09/05/WaRP7_Launch_and-_Preview/
date: 2016-09-05 09:52:01
author: andres
tags: [IMX7S, warp7]
categories: [New_Boards]
---

WaRP7 is a project from NXP in conjunction with Element14. The idea behind WaRP7 is to speed the development
of Internet of Things(IoT) and wearable devices by providing a small form factor board with the power and flexibility
required for a numerous variety of IoT projects.

# WaRP7 hardware overview:

<img src="{{ absolute_url }}/images/warp_boards.jpg">

WaRP7 has one main board which includes the following components:

* NXP i.MX7Solo (ARM Cortex A7 + ARM Cortex M4)
* Kingston 8GB eMMC and 512KB LPDDR3
* NXP PF3001 PMIC
* NXP BC3770 Battery charger IC
* Murata 1DX Wi-Fi + Bluetooth
* Omnivsion CSI camera module

The daughter card includes:

* NXP FXOS8700 accelerometer + magnetometer
* NXP FXAS21002 Gyroscope
* NXP MPL3115A2 pressure sensor
* NXP SGTL5000 audio codec
* NXP NT3H1101 NFC tag IC
* USB-serial interface
* MIPI Display
* MikroBUS™ interface

WaRP7’s i.MX7Solo chip provides a heterogeneous multicore architecture (A7+M4) which enables low-power modes critical
for most IoT end node and wearable designs, but also provides the power to drive a higher level operating system and a
rich user interface.

<img src="{{ site.url }}{{ site.baseurl }}/images/warp_chart.jpg">

External References:

<https://www.element14.com/warp7>
<https://github.com/WaRP7>
