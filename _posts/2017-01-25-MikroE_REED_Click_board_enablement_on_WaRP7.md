---
title: MikroE REED Click board enablement on WaRP7
date: 2017-01-25 10:12:00
author: andres
categories: [ Tutorial ]
tags: [warp7, REED, Click_boards]
---
The WaRP7 IO board includes a mikroBUS connector which allows a large amount of Click boards to interact with the i.MX7S chip. This post discusses the enablement and use of the REED Click board with WaRP7.

<center><img src="{{ absolute_url }}/images/REED_Click.JPG" alt="MikroE REED Click board" width="600"/></center>
<center><figcaption>MikroE REED Click board</figcaption></center>

The REED click board is a simple board that carries a standard (Single Pole Single Throw Normally Open) reed switch. A reed switch comprises of two thin magnetic contacts sealed inside a casing. One contact is a magnetic north pole, the other a south. The two contacts are separate, until a magnetic field is applied which snaps them close, activating the switch. A single mikroBUS pin (CS) connected to the WaRP7 outputting a 1 or 0 depending on the whether the switch is close or open. REED click is designed to use either a 3.3V or a 5V power supply.

<center><img src="{{ absolute_url }}/images/REED_Click_mounted.JPG" alt="MikroE REED Click board on WaRP7" width="600"/></center>
<center><figcaption>MikroE REED Click board on WaRP7</figcaption></center>

Once the board is connected, boot your WaRP7 board. After reaching the Linux prompt, the first step is to enable the GPIO pin to receive the input from the REED Click board. GPIO 119 is assigned to the mikroBUS pin (CS). Use the following commands for enablement:

```
$ cd /sys/class/gpio
$ echo 119 > export 
```

After this step get a magnet close to the switch. The L1 LED on the Click board turns on when the field is detected and the switch is closed. To read the status of the switch from the WaRP7 board use the following commands:

```
$ cd gpio119
$ echo in > direction
$ cat value 
```
A return value of 1 means that the switch is closed, a return value of 0 means it is open and no magnetic field is applied to the switch. 


