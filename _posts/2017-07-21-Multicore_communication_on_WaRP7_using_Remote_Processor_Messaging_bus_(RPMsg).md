---
title: Multicore communication on WaRP7 using Remote Processor Messaging bus (RPMsg)
date: 2017-07-21 14:16:58
author: vanessa
categories: [ Tutorial ]
tags: [ warp7, rpmsg, freertos, multicore, IMX7S ]
---
The Remote Processor Messaging (RPMsg) is a messaging bus that allows the
communication between cores in a multicore processor.

This tutorial shows how to use the RPMsg demo examples from `FreeRTOS_BSP_1.0.1_iMX7D`
BSP on WaRP7.

The software releases used in this tutorial are:
* [linux-warp7](https://github.com/WaRP7/linux-fslc);
* [FreeRTOS_BSP_1.0.1_iMX7D](https://www.nxp.com/webapp/Download?colCode=FreeRTOS_iMX7D_1.0.1_LINUX&appType=license&Parent_nodeId=1433867344829722440512&Parent_pageType=product&Parent_nodeId=1433867344829722440512&Parent_pageType=product).

{% capture notice-text %}
More information about the Linux releases available for WaRP7 can be found
[here](https://imxdev.gitlab.io/WaRP7_Linux_Kernel_status_including_upstream/).
{% endcapture %}

<div class="notice--success">
  <h4>NOTE</h4>
  {{ notice-text | markdownify }}
</div>
{: .notice--success}

# Kernel

The kernel needs to be customized to support RPMsg and Cortex-M4 on WaRP7.


- Apply the following patch to enable RPMsg support on WaRP7. Download this patch
<a href="{{absolute_url}}/patches/ARM-imx-Add-imx7s-warp-RPMsg-support.patch">
here</a>.

```diff
From 6cb41eba0996f05e7fa06b8e834516043145b96b Mon Sep 17 00:00:00 2001
From: Vanessa Maegima <vanessa.maegima@nxp.com>
Date: Wed, 23 Nov 2016 10:25:37 -0200
Subject: [PATCH 1/2] ARM: imx: Add imx7s-warp RPMsg support

The RPMsg driver is hardcoded to support imx7d-sdb.

Add support for imx7s-warp.

Signed-off-by: Vanessa Maegima <vanessa.maegima@nxp.com>
---
 arch/arm/mach-imx/imx_rpmsg.c | 7 +++++++
 1 file changed, 7 insertions(+)

diff --git a/arch/arm/mach-imx/imx_rpmsg.c b/arch/arm/mach-imx/imx_rpmsg.c
index 6e3aed1..4af740a 100644
--- a/arch/arm/mach-imx/imx_rpmsg.c
+++ b/arch/arm/mach-imx/imx_rpmsg.c
@@ -273,6 +273,7 @@ static struct imx_rpmsg_vproc imx_rpmsg_vprocs[] = {
 static const struct of_device_id imx_rpmsg_dt_ids[] = {
	{ .compatible = "fsl,imx6sx-rpmsg", },
	{ .compatible = "fsl,imx7d-rpmsg", },
+	{ .compatible = "fsl,imx7s-rpmsg", },
	{ /* sentinel */ }
 };
 MODULE_DEVICE_TABLE(of, imx_rpmsg_dt_ids);
@@ -293,6 +294,12 @@ static int imx_rpmsg_probe(struct platform_device *pdev)
				rpdev->vring[0] = 0xBFFF0000;
				rpdev->vring[1] = 0xBFFF8000;
			}
+			ret = of_device_is_compatible(np, "fsl,imx7s-rpmsg");
+			if (ret) {
+				/* hardcodes here now. */
+				rpdev->vring[0] = 0x9FFF0000;
+				rpdev->vring[1] = 0x9FFF8000;
+			}
		} else {
			break;
		}
--
2.7.4
```

- Apply the following patch to add the Cortex-M4 support on WaRP7. Download this
patch <a href="{{absolute_url}}/patches/ARM-dts-imx7s-warp-m4-Add-i.MX7s-M4-support.patch">
here</a>.

```diff
From 038e88ce99724e7f57afcd6694f83009bb1551e8 Mon Sep 17 00:00:00 2001
From: Vanessa Maegima <vanessa.maegima@nxp.com>
Date: Wed, 23 Nov 2016 10:32:02 -0200
Subject: [PATCH 2/2] ARM: dts: imx7s-warp-m4: Add i.MX7s M4 support

Add support for Cortex-M4 on i.mx7s-warp.

Signed-off-by: Vanessa Maegima <vanessa.maegima@nxp.com>
---
 arch/arm/boot/dts/Makefile           |  3 ++-
 arch/arm/boot/dts/imx7s-warp-m4.dts  |  9 +++++++++
 arch/arm/boot/dts/imx7s-warp-m4.dtsi | 26 ++++++++++++++++++++++++++
 3 files changed, 37 insertions(+), 1 deletion(-)
 create mode 100644 arch/arm/boot/dts/imx7s-warp-m4.dts
 create mode 100644 arch/arm/boot/dts/imx7s-warp-m4.dtsi

diff --git a/arch/arm/boot/dts/Makefile b/arch/arm/boot/dts/Makefile
index 8c18e59..d1de4c7 100644
--- a/arch/arm/boot/dts/Makefile
+++ b/arch/arm/boot/dts/Makefile
@@ -401,7 +401,8 @@ dtb-$(CONFIG_SOC_IMX7D) += \
	imx7d-sdb-reva-qspi.dtb \
	imx7d-sdb-reva-touch.dtb \
	imx7d-sdb-reva-wm8960.dtb \
-	imx7s-warp.dtb
+	imx7s-warp.dtb \
+	imx7s-warp-m4.dtb
 dtb-$(CONFIG_SOC_LS1021A) += \
	ls1021a-qds.dtb \
	ls1021a-twr.dtb
diff --git a/arch/arm/boot/dts/imx7s-warp-m4.dts b/arch/arm/boot/dts/imx7s-warp-m4.dts
new file mode 100644
index 0000000..53cbe95
--- /dev/null
+++ b/arch/arm/boot/dts/imx7s-warp-m4.dts
@@ -0,0 +1,9 @@
+/*
+ * Copyright (C) 2016 Freescale Semiconductor, Inc.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 as
+ * published by the Free Software Foundation.
+ */
+#include "imx7s-warp.dts"
+#include "imx7s-warp-m4.dtsi"
diff --git a/arch/arm/boot/dts/imx7s-warp-m4.dtsi b/arch/arm/boot/dts/imx7s-warp-m4.dtsi
new file mode 100644
index 0000000..b856fec
--- /dev/null
+++ b/arch/arm/boot/dts/imx7s-warp-m4.dtsi
@@ -0,0 +1,26 @@
+/*
+ * Copyright (C) 2016 Freescale Semiconductor, Inc.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 as
+ * published by the Free Software Foundation.
+ */
+
+/ {
+	memory {
+		linux,usable-memory = <0x80000000 0x1ff00000>;
+	};
+	m4_tcm: tcml@007f8000 {
+		compatible = "fsl, m4_tcml";
+		reg = <0x007f8000 0x8000>;
+	};
+};
+
+&ocram {
+	reg = <0x00901000 0xf000>;
+};
+
+&rpmsg{
+	compatible = "fsl,imx7s-rpmsg";
+	status = "okay";
+};
--
2.7.4
```

## Building Linux

To build the Linux image, follow the next command lines:

```
$ make imx_v7_defconfig
$ make
```

Copy **imx7s-warp-m4.dtb** and **zImage** to the boot partition of the SD card.

# FreeRTOS

The RPMsg buffer (VRING) address needs to be customized in the FreeRTOS source
code to the proper values used by WaRP7.

The VRING address is defined at: `FreeRTOS_BSP_1.0.1_iMX7D/middleware/multicore/open-amp/porting/imx7d-m4/platform_info.c`.

On i.MX7D-SabreSD:

```
#define VRING0_BASE     0xBFFF0000
#define VRING1_BASE     0xBFFF8000
```

For WaRP7, change these values to:

```
#define VRING0_BASE     0x9FFF0000
#define VRING1_BASE     0x9FFF8000
```

## Building FreeRTOS

There are four demos to test the RPMsg messaging in this BSP:
* `pingpong_bm`
* `pingpong_freertos`
* `str_echo_bm`
* `str_echo_freertos`

In this tutorial, the `pingpong_freertos` demo is used, but the same procedure
can be done for all four demos.

- First, make sure `cmake` is installed:

```
$ sudo apt-get install cmake
```

- Then download the [gcc-arm-none-eabi](https://launchpad.net/gcc-arm-embedded/5.0/5-2016-q2-update/+download/gcc-arm-none-eabi-5_4-2016q2-20160622-linux.tar.bz2)
toolchain and export its location as ARMGCC_DIR:

```
$ export ARMGCC_DIR=<toolchain_dir>
```

- Enter the demo `armgcc` directory and run the build command:

```
$ cd <freertos_dir>/examples/imx7d_sdb_m4/demo_apps/rpmsg/pingpong_freertos/armgcc
$ ./build_all.sh
```

- The previous command generates the debug and release binaries (.bin files) to
boot the M4 core. These files are located in the `debug` and in the `release`
folders. Copy one of these binaries to the boot partition of the SD card.

# Running the demo

- Open two serial consoles, one at `/dev/ttyUSB0` for Cortex-A7 to boot Linux
and other at `/dev/ttyUSB1` for Cortex-M4 to boot FreeRTOS.

- Power up the board and stop at U-boot on the A7 console.

- Enter the following commands to load the FreeRTOS binary to M4.

```
=> fatload mmc 0:1 0x7F8000 rpmsg_pingpong_freertos_example.bin
=> dcache flush
=> bootaux 0x7F8000
## Starting auxiliary core at 0x007F8000 ...
```

{% capture notice-text2 %}
Remember to update the binary name if running a different demo.
{% endcapture %}

<div class="notice--success">
  <h4>NOTE</h4>
  {{ notice-text2 | markdownify }}
</div>
{: .notice--success}

- The M4 console will print the demo information.

```
RPMSG PingPong FreeRTOS RTOS API Demo...
RPMSG Init as Remote
```

- Run the following command on the A7 console to boot Linux:

```
=> run bootcmd
```

- In the Linux user space, enter the following command to start the communication
between cores:

```
# modprobe imx_rpmsg_pingpong
```

{% capture notice-text3 %}
The command above is used only for the `pingpong` demos. For the `str_echo` demos,
use:
```
# modprobe imx_rpmsg_tty
```
{% endcapture %}

<div class="notice--success">
  <h4>NOTE</h4>
  {{ notice-text3 | markdownify }}
</div>
{: .notice--success}

- A7 and M4 cores will send messages to each other.

<figure>
<img src="{{ absolute_url }}/images/rpmsg_demo.png" alt="Multicore communication" align="middle"/>
    <figcaption align="middle">
			Multicore communication (left: A7, right: M4)
		</figcaption>
</figure>
