---
title: How to use OV5640 with i.MX 6UltraLite EVK
date: 2017-02-23 16:11:35
author: marco
categories: [ Tutorial ]
tags: [ L4.1.15_2.0.0_GA, GStreamer, IMX6UL, ov5640, SCH700-27820, FX12B-40P-0.4SV ]
---
The i.MX 6UltraLite EVK kernel release includes the parallel camera `OV5640` support by default. However, depending on the base board revision, this camera might requires the `SCH700-27820` adapter below:

<center><img src="{{ absolute_url }}/images/camera_adapter.png" alt="Camera Adapter"  width="300"/></center>
<center><figcaption>Camera Adapter</figcaption></center>

According to the following image, the `FX12B-40P-0.4SV` connector layout was changed in the RevC, giving the possibility to use the OV5640 directly:

<center><img src="{{ absolute_url }}/images/revb_revc_comparison.jpg" alt="RevB vs RevC camera layout"  width="640"/></center>
<center><figcaption>Rev B in the left and Rev C in the right, which does not need the adapter</figcaption></center>

So if you are using a base board older than Rev C, plug in the camera with the adapter and connect the other end of the adapter to the board:

<center><img src="{{ absolute_url }}/images/camera_position.png" alt="Camera position" width="300"/></center>
<center><figcaption>Camera position</figcaption></center>

In order to use the parallel OV5640 camera, it's necessary to set up the environment variable below on U-Boot, independent of the base board revision:

```
=> setenv fdt_file ‘imx6ul-14x14-evk-csi.dtb’
=> saveenv
```

Follow the GStreamer pipeline example to test the camera connection:
```
$ gst-launch-1.0 v4l2src device=/dev/video1 ! video/x-raw,width=640,height=480 ! autovideosink
```

<center><img src="{{ absolute_url }}/images/camera_test.png" alt="Camera test" width="320"/></center>
<center><figcaption>RevB with adapter camera test</figcaption></center>

<center><img src="{{ absolute_url }}/images/camera_test_revc.jpg" alt="Camera test" width="320"/></center>
<center><figcaption>RevC directly camera test</figcaption></center>

This test was done using the kernel BSP release 4.1.15-2.0.0 GA.
