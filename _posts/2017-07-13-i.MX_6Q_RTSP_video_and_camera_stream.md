---
title: i.MX 6DL/Q SABRE-SD RTSP video and camera stream
date: 2017-07-13 13:47:31
author: marco
categories: [ Tutorial ]
tags: [ yocto, VPU, GStreamer, rtsp, H264, encoder, decoder, IMX6Q, IMX6DL ]
---
This article shows how to use the open source **gstreamer1.0-rtsp-server** package on i.MX6DL/Q SABRE-SD to stream video files and camera using RTSP protocol. It is based on
[i.MX_Linux_User's_Guide Rev.4.1.15._2.0.0-ga](https://community.nxp.com/servlet/JiveServlet/download/913332-1-405117/i.MX_Linux_User's_Guide.pdf), ```Multimedia``` section.  
To install gstreamer1.0-rtsp-server in any Yocto Project image, please follow the steps below:
1. Enable meta-multimedia layer:  
Add the following on your `build/conf/bblayers.conf`:
```
BBLAYERS += "$"${BSPDIR}/sources/meta-openembedded/meta-multimedia"
```
2. Include gstreamer1.0-rtsp-server into the image:  
Add the following on your `build/conf/local.conf`:
```
IMAGE_INSTALL_append += "gstreamer1.0-rtsp-server"
```
3. Run ```bitbake``` and mount your ```sdcard```.

4. Copy the binaries:  
Access the gstreamer1.0-rtsp-server examples folder:
```
$ cd /build/tmp/work/cortexa9hf-vfp-neon-poky-linux-gnueabi/gstreamer1.0-rtsp-server/$version/build/examples/.libs
```
Copy the **test-uri** and **test-launch** to the rootfs ```/usr/bin``` folder.
```
$ sudo cp test-uri test-launch /media/USER/ROOTFS_PATH/usr/bin
```

## Preparing the boards

The video/camera is streamed by the server (i.MX 6Q SABRE-SD at left) to the client (i.MX 6DL SABRE-SD at right), as the image below:
<img src="{{ site.url }}{{ site.baseurl }}/images/camera-rtsp-streaming.jpg">

To connect more than one board to the minicom, open it with the command:
```
$ sudo minicom –s
```
Enter in the ```Serial port setup``` option and press ```A``` to set or change the PORTNUMBER, in "ttyUSB$PORTNUMBER”.

Be sure that the IPs are correctly set:  
* SERVER:
```
=> ifconfig eth0 $SERVERIP
```
* CLIENT:
```
=> ifconfig eth0 $CLIENTIP
```

## GStreamer pipelines

### Video file example
* SERVER:
```
=> test-uri file:///home/root/video_file.mp4
```
* CLIENT:
```
=> gst-launch-1.0 playbin uri=rtsp://$SERVERIP:8554/test
```
You can try to improve the ```framerate``` performance using manual pipelines in the CLIENT with the ```rtspsrc``` plugin instead of playbin. Follow an example:
```
=> gst-launch-1.0 rtspsrc location=rtsp://$SERVERIP:8554/test caps = 'application/x-rtp'  ! queue max-size-buffers=0 ! rtpjitterbuffer latency=100 ! queue max-size-buffers=0 ! rtph264depay ! queue max-size-buffers=0 ! decodebin ! queue max-size-buffers=0 ! imxv4l2sink sync=false
```

### Camera example
* SERVER:
```
=> test-launch "( imxv4l2src device=/dev/video0 ! capsfilter caps='video/x-raw, width=1280, height=720, framerate=30/1, mapping=/test' ! vpuenc_h264 ! rtph264pay name=pay0 pt=96 )"
```
* CLIENT:
```
=> gst-launch-1.0 rtspsrc location=rtsp://$SERVERIP:8554/test ! decodebin ! autovideosink sync=false
```
The ```rtspsrc``` has two properties very useful for RTSP streaming:
* **Latency**: Useful for low-latency RTSP stream playback (default 200 ms);
* **Buffer-mode**: Used to control buffer mode. The ```slave``` mode is recommended for low-latency communications.  

Using these properties, the example below gets **29 FPS** without a ```sync=false``` property in the sink plugin. The key achievement here is the fact that there is no dropped frame:
```
=> gst-launch-1.0 rtspsrc location=rtsp://$SERVERIP:8554/test latency=100 buffer-mode=slave ! queue max-size-buffers=0 ! rtph264depay ! vpudec ! imxv4l2sink
```
