---
title: Profiling iMX Application Power Consumption with Kinetis
date: 2017-11-07 16:31:34
author: david
categories: [ANN]
tags: [power, current]
---
Measuring the power consumed by i.MX application processor in a system does not
necessarily require fancy, multi-thousand dollar tools. This article details a
four-channel power measurement tool can be hand-built in your lab for a fraction
of the cost of a commercial measurement system. While it may not be as fast or
as accurate as a commercial system, it’s a very useful tool that can be easily
built and used to measure run, idle and low power mode power consumption.

<center><img src="{{ absolute_url }}/images/profiler-sensor.jpg" width="80%"/></center>

At the heart of this measurement system are a pair of shunt resistors for each
rail measure current via a sense amplifiers. One shunt is used for the high
current range and the other for the low range. The ADCs of a Kinetis
microcontroller measure the sense amp outputs and the rail voltages (that’s how
we get power), with GPIO controlling FETs that perform the range switching for
each rail. The measurement data is collected with a PC using a USB virtual COM
port.

The article ([here](https://community.nxp.com/docs/DOC-335567)) contains the
theory of operation, and assembly and instrumentation tips. Basic source code
that controls range switching and makes the measurements is included. In
addition to the schematics, layout and bill of materials, links are provided for
ordering the low cost PCBs.
