---
title: VPU H.264 transcoding streamer
redirect_from: blog/tutorial/2016/09/30/VPU_h264_transcoding_streamer/
date: 2016-09-30 16:11:32
author: marco
categories: [ Tutorial ]
tags: [ VPU, GStreamer, tcp, H264, encoder, decoder, transcoder, IMX6Q, IMX6DL ]
---
This article shows how to use the i.MX6DL/Q to transcode and stream videos on 1080i/p @ 24fps and 720p @ 30fps. For this test, we used one i.MX6DL as server and an i.MX6DL and i.MX6Q as clients. The video is streamed by the server, playing the sound at the same time, while the clients show the video in the HDMI output, as the image below:

<img src="{{ site.url }}{{ site.baseurl }}/images/video-streaming.jpg">

This test depends on some GStreamer plugins. To check that the right GStreamer plugins are installed type the following commands:
```
$ gst-inspect-1.0 | grep h264
```
To return all the H.264 related plugins, or:
```
$ gst-inspect-1.0 decodebin
```
To check directly the command.

To connect more than one board to the minicom, open it with the command:
```
$ sudo minicom –s
```
This way, you open the configuration menu. Enter in the "Serial port setup" option and press "A" to set or change the PORTNUMBER, in "ttyUSB$PORTNUMBER”.

To set the HDMI output, run the commands below from the U-Boot prompt:
```
=> setenv mmcargs 'setenv bootargs console=ttymxc0,115200 root=/dev/mmcblk2p2 rootwait rw video=mxcfb0:dev=hdmi,1920x1080M@60,if=RGB24 consoleblank=0'
=> saveenv
```
Be sure that the IPs are correctly setted:
SERVER:
```
=> ifconfig eth0 $SERVERIP
```
CLIENTS:
```
=> ifconfig eth0 $CLIENTSIP
```

## Streaming transcoded video only

SERVER:
```
=> gst-launch-1.0 filesrc location=/home/root/bbb_720p.mp4 ! decodebin ! queue max-size-buffers=0 ! vpuenc_h264 gop-size=2 bitrate=20000 ! queue max-size-buffers=0 ! rtph264pay config-interval=2 ! queue max-size-buffers=0 ! gdppay ! tcpserversink blocksize=512000 host=$SERVERIP$ port=8554
```
CLIENTS:
```
=> gst-launch-1.0 tcpclientsrc host=$SERVERIP$ port=8554 ! gdpdepay ! queue max-size-buffers=0 ! 'application/x-rtp, media=(string)video, clock10-rate=(int)90000, payload=(int)96' ! queue max-size-buffers=0 ! rtpjitterbuffer latency=100 ! queue max-size-buffers=0 ! rtph264depay ! queue max-size-buffers=0 ! decodebin ! autovideosink sync=false
```

## Streaming transcoded video + audio

SERVER:
```
=> gst-launch-1.0 filesrc location=/home/root/bbb_720p.mp4 ! decodebin name=demux demux. ! queue max-size-buffers=0 ! vpuenc_h264 gop-size=2 bitrate=20000 ! queue max-size-buffers=0 ! rtph264pay config-interval=2 ! queue max-size-buffers=0 ! gdppay ! tcpserversink blocksize=512000 host=$SERVERIP$ port=8554 demux. ! alsasink
```
CLIENTS:
```
=> gst-launch-1.0 tcpclientsrc host=$SERVERIP$ port=8554 ! gdpdepay ! queue max-size-buffers=0 ! 'application/x-rtp, media=(string)video, clock10-rate=(int)90000, encoding-name=(string)H264' ! queue max-size-buffers=0 ! rtpjitterbuffer latency=100 ! queue max-size-buffers=0 ! rtph264depay ! queue max-size-buffers=0 ! decodebin ! autovideosink sync=false
```

You can check the results at the movie below, with the 1080p@24fps Big Bucky Bunny video clip:

<iframe width="560" height="315" src="https://www.youtube.com/embed/uiSCHTywlU0" frameborder="0" allowfullscreen></iframe>
