---
title: How to use the meta-warp7-distro
date: 2017-10-03 20:00:00
author: pierre-jean
categories: [ Tutorial ]
tags: [ warp7, yocto, Click_boards, bluetooth, Qt5, IMX7S ]
---

This article is intented to present and explain how to work with the **meta-warp7-distro**, which is a new dedicated `DISTRO` layer for the WaRP7.

{% include toc icon="th-list" title="Summary" %}

The Layers used in this post are:

* [meta-warp7-distro](https://github.com/WaRP7/meta-warp7-distro);
* [meta-swupdate](https://github.com/sbabic/meta-swupdate);
* [meta-qt5](https://github.com/meta-qt5/meta-qt5);
* [meta-openembedded](https://github.com/openembedded/meta-openembedded);

# The content

The **meta-warp7-distro** provides several parts such as:

## Class

A single class `base-image.bbclass`, to handle the image configuration. This includes for example:

* SWUpdate (to manage [software updade](http://sbabic.github.io/swupdate/index.html))
* Alsa (utils and tools)
* dnsmasq and hostapd (needed if you want to turn on the Hotspot)
* bluez5 (To play with Bluetooth protocol)
* mosquitto (libs and associated tools -> `mosquitto_pub` and `mosquitto_sub`)
* An SSH (dropbear) server and a sftp server (to deploy Qt5 Application via QtCreator)
* There are also packages like `i2c-tools`, `spitools`, `dbus` or `evtest`

## Images

[2 Images](https://github.com/WaRP7/meta-warp7-distro/tree/master/recipes-warp7/images) are available:

* base-image.bb (inherit of `base-image` class)
* qt5-image (with qtbase, qtbase-*, qtserialbus modules)

## Kernel

The bbappend file allows to support new features like:

* gpio-exporter driver ([see documentation](https://github.com/WaRP7/meta-warp7-distro/blob/master/recipes-kernel/linux/linux-warp7/imx7s-warp/0001-Doc-DT-Add-binding-document-for-GPIO-exporter.patch))
* Cortex M4 support (Thanks to [Vanessa](https://www.linkedin.com/in/vanessa-maegima/) for [this](https://imxdev.gitlab.io/tutorial/Multicore_communication_on_WaRP7_using_Remote_Processor_Messaging_bus_%28RPMsg%29/))

The **gpio-exporter** is used by init script to export all GPIO's into `/dev/mikrobus/gpio_name`, as defined in device-tree source file.
{: .notice--info}

## Userspace Application

Furthermore, some application are provided to work with the MikroBUS Socket and click modules (air Quality 2 click, DAC 3 Click, ...)

# Building the qt image

To build the qt image, follow the next command lines.

First, install the `repo` tool (very useful to manage several `git` repository): 
```console
$: mkdir ~/bin
$: curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$: chmod a+x ~/bin/repo
```

Then, you have to download the BSP (from FSL community)
```console
$: PATH=${PATH}:~/bin
$: mkdir warp7_bsp
$: cd warp7_bsp
$: repo init -u https://github.com/Freescale/fsl-community-bsp-platform -b pyro (replace pyro by master if you want to test master instead)
$: repo sync
```

Now, it's time to download the **meta-warp7-distro** and its dependencies (always on **pyro** branch)
```console
$: cd sources
$: git clone -b pyro https://github.com/WaRP7/meta-warp7-distro.git
$: git clone -b pyro https://github.com/meta-qt5/meta-qt5.git
$: git clone -b pyro https://github.com/sbabic/meta-swupdate.git
$: cd ../
```

Once is done, you can create your environment (**Machine** + **Distribution**) via the following command line:
```console
$: MACHINE=imx7s-warp DISTRO=warp7 source setup-environment warp7-build/ 
```

* `MACHINE` : imx7s-warp, specify the target.
* `DISTRO` : warp7, to use the [specifc](https://github.com/WaRP7/meta-warp7-distro/blob/master/conf/distro/warp7.conf) configuration available inside the layer.
* `warp7-build/`, where you want to build.


It's now the moment to prepare your build. For this, you must include all layers that you need, either by the `conf/bblayers.conf` file or by directly via the `bitbake-layers` util:
```console
$: bitbake-layers add-layer ../sources/meta-qt5/ 
$: bitbake-layers add-layer ../sources/meta-warp7-distro/
$: bitbake-layers add-layer ../sources/meta-swupdate/ 
$: bitbake-layers add-layer ../sources/meta-openembedded/meta-python/ 
$: bitbake-layers add-layer ../sources/meta-openembedded/meta-networking/
```

Let's build with `bitbake`
```console
$: bitbake qt5-image
```

After a long while, you can find generated files (Bootloader, Root-Filesystem and so on) inside the following directory : `warp7-build/tmp/deploy/images/imx7s-warp`

You can now flashing your board through the bootloader (u-boot) and the `ums` utility to mount the eMMC. 

For further information about **Flashing procedure**, follow this [documentation](https://github.com/WaRP7/WaRP7-User-Guide/blob/master/06-Chapter/Yocto_Project.adoc#steps-to-update-the-image).
{: .notice--info}


Some examples on next articles !
