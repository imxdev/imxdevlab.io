---
title: i.MX 8QXP MEK launch and preview
redirect_from: blog/tutorial/2018/11/07/system-controller-unit-introduction-for-imx-8qxp-mek/
date: 2018-11-07 15:34:56
author: andres
tags: [IMX8QXP]
categories: [Tutorial]
---

The i.MX 8QXP MEK is the latest release from NXP's i.MX 8 family. This is the most powerful i.MX to date and promises capabilities directed to the industrial and automotive industries.

The i.MX 8QXP MEK has CPU board has the following characteristics:

* NXP i.MX8QXP (4X ARM Cortex A35 + 1 ARM Cortex M4)
* 32-bit LPDDR4 memory (DDR3L w/ ECC option available)
* 1 x GC7000Lite
* NXP PF8100 PMIC
* 1 x USB3 OTG w/PHY
* Tensilica HiFi4 DSP
* VPU 4K h.265 decode
* VPU 1080p h.264 enconde
* 2 x MIPI DSI/LVDS combo output (required daughter card)
* 1 x MIPI CSI input

<img src="{{ site.url }}{{ site.baseurl }}/images/imx8qxp-top.jpg">

Additionally the i.MX 8QXP MEK can be connected to the MCIMX8-8X-BB baseboard. This baseboard provides the following expansions devices:

* 1 x I2C auxiliary connector
* 1 x tamper header
* 1 x parallel CSI connector
* 1 x UART
* 2 x CAN connectors
* 1 x Audio in/out connector (CS42888 audio card required)(bundled)
* 1 x 10/100/1000 ethernet connector (muxed w/ audio port)
* 1 x MikroBUS™ interface
* 1 x Arduino connector

<img src="{{ site.url }}{{ site.baseurl }}/images/imx8qxp-baseboard.jpg">

Full block diagram below:

<img src="{{ site.url }}{{ site.baseurl }}/images/8qxp_block_diagram.png">




