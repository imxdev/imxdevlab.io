---
title: Enabling Cortex-M4 on i.MX 8QXP MEK
date: 2018-11-08 16:48:19
author: vanessa
categories: [ tutorial ]
tags: [ m4, freertos, sdk, mcuxpresso, IMX8QXP ]
---

The `i.MX 8QuadXPlus Multisensory Enablement Kit (MEK)` is a NXP development
platform based on Cortex A-35 + Cortex-M4 cores. Built with high-level integration
to support graphics, video, image processing, audio, and voice functions, the i.MX
8X processor family is ideal for safety-certifiable and efficient performance
requirements.

This tutorial shows how to enable the Cortex-M4 using the MCUXpresso SDK package
and loading the binary from the network.

{: .notice--info}
**NOTE**: It is also possible to load the Cortex-M4 image from the SCFW using the
`imx-mkimage` utility. Please see next blog posts for this tutorial.

# Setting up the machine

{: .notice--info}
**NOTE**: This shows the procedure for a Linux environment. For the Windows OS,
please see the **Getting Started** documentation on the SDK package.

- Install `cmake` on the host machine:

```
$ sudo apt-get install cmake
```

- Download the `armgcc` toolchain from
[here](https://launchpad.net/gcc-arm-embedded/5.0/5-2016-q2-update/+download/gcc-arm-none-eabi-5_4-2016q2-20160622-linux.tar.bz2)
and export the location as `ARMGCC_DIR`:

```
$ export ARMGCC_DIR=<your_path_to_arm_gcc>/gcc-arm-none-eabi-5_4-2016q2/
```

{: .notice--info}
**NOTE**: The `ARMGCC_DIR` variable needs to be exported on the terminal used
for compilation.

To setup the TFTP server on the host machine, follow the first two sections on
[this](https://imxdev.gitlab.io/tutorial/How_to_boot_Kernel_and_File_System_from_the_network/)
blog post (*Configuring your Host PC for TFTP* and *Configuring your Host PC for NFS*).

# Downloading the SDK

Download the [MCUXpresso SDK](https://mcuxpresso.nxp.com/en/welcome) following
these steps:

- Click on "*Select Development Board*";

- Select `MEK-MIMX8QX` under "*Select a Device, Board, or Kit*" and click on
"*Build MCUXpresso SDK*" on the right;

- Select "*Host OS*" as `Linux` and "*Toolchain/IDE*" as `GCC ARM Embedded`;

- Add "FreeRTOS" and all the wanted Middleware and hit "*Request Build*";

- Wait for the SDK to build and download the package.

# Building the image

All demos and code examples available on the SDK package are located in the
directory `<<SDK_dir>>/boards/mekmimx8qx/`. This tutorial shows how to build
and flash the `hello_world` demo but similar procedures can be applied for any
example (demo, driver, multicore, etc) on the SDK.

- To build the demo, enter the *armgcc* folder under the demo directory and make
sure that the `ARMGCC_DIR` variable is set correctly.

```
$ cd ~/SDK_2.3.0_MEK-MIMX8QX/boards/mekmimx8qx/demo_apps/hello_world/armgcc
$ export ARMGCC_DIR=<your_path_to_arm_gcc>/gcc-arm-none-eabi-5_4-2016q2/
```

- Run the `build_release.sh` script to build the code.

```
$ ./build_release.sh
```

{: .notice--info}
**NOTE**: If needed, give the script execution permission by running `chmod +x build_release.sh`.

- This generates the M4 binary (`hello_world.bin`) under the *release* folder. Copy
this image to the */tftpboot/* directory on the host PC.

{: .notice--info}
**NOTE**: This procedure shows how to build the M4 image that runs on TCM. To run
the image from DDR, use the `build_ddr_release.sh` script to build the binary under
the *ddr_release* folder.

# Flashing the image

- Open two serial consoles, one for `/dev/ttyUSB0` for Cortex-A35 to boot Linux,
and one for `/dev/ttyUSB1` for Cortex-M4 to boot the SDK image.

- On the A35 console, with a SD Card with U-Boot, stop the booting process and
enter the following commands to load the M4 binary to TCM:

```
=> dhcp
=> setenv serverip <ip_from_host_pc>
=> tftp 0x88000000 hello_world.bin
=> dcache flush
=> bootaux 0x88000000
```

Then the M4 core will load the image to the `/dev/ttyUSB1` console.
