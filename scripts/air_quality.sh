# /bin/sh

while :
do
        num=$(cat /sys/bus/iio/devices/iio\:device0/in_voltage0_raw)
        echo "$(($num * 1800 / 4095)) ppm"
        sleep 1
done
