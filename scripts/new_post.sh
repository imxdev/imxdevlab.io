#!/bin/sh
#
# Shell script for creating post
# based on jekyll's layout format

# get created date
date=`date +%Y-%m-%d`
datetime=`date "+%Y-%m-%d %H:%M:%S"`

# get file title from command line
echo "The Post Title is the text used to create the URL, be wise"
read -p "   Post Title: " title
echo "\n\nYou can leave Author empty (guest), but you will still be the commiter"
echo "For the first time author, remember to add your description on file _data/authors.yml"
read -p "   Author: " author

# create file title
title_=$(echo $title | sed -e 's/ /_/g')
filetitle=$date-$title_.md

# create file
touch _posts/$filetitle

# check if there author is empty=guest
if [ -z "$author" ]; then
   author="guest"
fi

# write default layout to the file
echo '---' >> _posts/$filetitle
echo 'title: '$title >> _posts/$filetitle
echo 'date: '$datetime >> _posts/$filetitle
echo 'author: '$author >> _posts/$filetitle
echo 'categories: [ ]' >> _posts/$filetitle
echo 'tags: [ ]' >> _posts/$filetitle
echo '---' >> _posts/$filetitle
echo '{% include toc %}' >> _posts/$filetitle
echo '<center><img src="{{ absolute_path }}/images/imx.png" width="80%"/></center>' >> _posts/$filetitle
echo ""
echo "You can find your new post under _posts/"
echo ""
echo "Next steps:"
echo "$ git add _posts/"$filetitle
echo "$ git commit -s -m \"Add new post\""
echo "$ git push origin master:master"
