# /bin/sh

if [ -z "$1" ];  then
   echo "Usage: "
   echo "${0##*/} <input-file.md>"
   exit
fi

pandoc -V geometry:"top=2cm, bottom=1.5cm, left=1.5cm, right=1.5cm" -o output.pdf $1

